package utsugi_kenta.bulletin_board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import utsugi_kenta.bulletin_board.beans.User;
import utsugi_kenta.bulletin_board.service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		User user = new UserService().select(account, password);

		if (!isValid(user, account, password, errorMessages)) {
			errorMessages.add("ログインに失敗しました");
			request.setAttribute("loginAccount", account);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		HttpSession session = request.getSession();
		session.setAttribute("loginUser", user);
		response.sendRedirect("./");
	}

	private boolean isValid(User user, String account, String password, List<String> errorMessages) {

		try {
			if(StringUtils.isBlank(account) || StringUtils.isBlank(password)) {
				if (StringUtils.isBlank(account)) {
					errorMessages.add("アカウント名を入力してください");
				}
				if(StringUtils.isBlank(password)) {
					errorMessages.add("パスワードを入力してください");
				}
			}else {
				int isStopped = user.getIsStopped();
				if(isStopped == 1) {
					errorMessages.add("現在このアカウントは停止状態です");
				}
			}
		}catch(NullPointerException e) {
			errorMessages.add("登録しているアカウントまたはパスワードが間違っています");
		}

		if (errorMessages.size() != 0) {
		    return false;
		}
		return true;
	}
}