package utsugi_kenta.bulletin_board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import utsugi_kenta.bulletin_board.beans.Branch;
import utsugi_kenta.bulletin_board.beans.Department;
import utsugi_kenta.bulletin_board.beans.User;
import utsugi_kenta.bulletin_board.service.BranchService;
import utsugi_kenta.bulletin_board.service.DepartmentService;
import utsugi_kenta.bulletin_board.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignupServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();
		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		    throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();
		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();

		User user = getUser(request);
		String checkPassword = request.getParameter("checkPassword");

		if (!isValid(user, checkPassword, errorMessages)) {
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("user", user);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}
		new UserService().insert(user);
		response.sendRedirect("./management");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {
		User user = new User();
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
		return user;
	}

	private boolean isValid(User user, String checkPassword, List<String> errorMessages) {

		String account = user.getAccount();
		String password = user.getPassword();
		String name = user.getName();
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();

		User accountJudge = new UserService().select(account);

		if (StringUtils.isBlank(name)) {
		    errorMessages.add("名前が空白のまま登録できません");
		}else if(name.length() > 10) {
		    errorMessages.add("名前は10文字以下で入力してください");
		}

		if (StringUtils.isBlank(account)) {
		    errorMessages.add("アカウントが空白のまま登録できません");
		} else if ((account.length() < 6 || account.length() > 20) && (!account.matches("^[a-zA-Z0-9]{6,20}$"))) {
		    errorMessages.add("アカウントは6文字以上20文字以下の半角英数字で入力してください");
		}

		if(accountJudge != null) {
			errorMessages.add("このアカウントは既に使われています");
		}

		if (!password.equals(checkPassword)) {
			errorMessages.add("パスワードが確認用パスワードと一致していません");
		}

		if (StringUtils.isBlank(password)) {
		    errorMessages.add("パスワードと確認パスワードが空白のまま登録できません");
		}else if((password.length() < 6 || password.length() > 20) && (!password.matches("^[!-~]{6,20}$"))) {
			errorMessages.add("パスワードは6文字以上20文字以下の半角記号英数字で入力してください");
		}

		if((branchId == 1 && departmentId >= 3 )|| (branchId != 1 && departmentId <= 2 )) {
			errorMessages.add("支社と部署の組み合わせが不正です");
		}

		if (errorMessages.size() != 0) {
		    return false;
		}
		return true;
	}
}
