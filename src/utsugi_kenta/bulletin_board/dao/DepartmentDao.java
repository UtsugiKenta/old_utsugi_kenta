package utsugi_kenta.bulletin_board.dao;

import static utsugi_kenta.bulletin_board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import utsugi_kenta.bulletin_board.beans.Department;
import utsugi_kenta.bulletin_board.exception.SQLRuntimeException;

public class DepartmentDao {
	public List<Department> select(Connection connection) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM departments ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<Department> branches = toDepartments(rs);
			return branches;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Department> toDepartments(ResultSet rs) throws SQLException {

		List<Department> departments = new ArrayList<Department>();
		try {
			while (rs.next()) {
				Department department = new Department();
				department.setId(rs.getInt("id"));
				department.setName(rs.getString("name"));
				department.setCreatedDate(rs.getDate("created_date"));
				department.setUpdatedDate(rs.getDate("updated_date"));

				departments.add(department);
			}
			return departments;
		} finally {
			close(rs);
		}
	}
}
