package utsugi_kenta.bulletin_board.dao;

import static utsugi_kenta.bulletin_board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import utsugi_kenta.bulletin_board.beans.UserBranchDepartment;
import utsugi_kenta.bulletin_board.exception.SQLRuntimeException;

public class UserBranchDepartmentDao {
	public List<UserBranchDepartment> select(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("	users.id as userId, ");
			sql.append("	users.account as userAccount, ");
			sql.append("	users.name as userName, ");
			sql.append("	users.is_stopped as isStopped, ");
			sql.append("	branches.name as branchName, ");
			sql.append("	departments.name as departmentName ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("JOIN departments ");
			sql.append("ON users.department_id = departments.id ");
			sql.append("ORDER BY userId ASC ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserBranchDepartment> userStatusList = toUserStatus(rs);
			return userStatusList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserBranchDepartment> toUserStatus(ResultSet rs) throws SQLException {

		List<UserBranchDepartment> userStatusList = new ArrayList<UserBranchDepartment>();
		try {
			while (rs.next()) {
				UserBranchDepartment userStatus = new UserBranchDepartment();
				userStatus.setUserId(rs.getInt("userId"));
				userStatus.setUserAccount(rs.getString("userAccount"));
				userStatus.setUserName(rs.getString("userName"));
				userStatus.setBranchName(rs.getString("branchName"));
				userStatus.setDepartmentName(rs.getString("departmentName"));
				userStatus.setIsStopped(rs.getInt("isStopped"));

				if(rs.getInt("isStopped") == 0) {
					userStatus.setStopStatus("利用可能");
				}else {
					userStatus.setStopStatus("停止中");
				}
				userStatusList.add(userStatus);
			}
			return userStatusList;
		} finally {
			close(rs);
		}
	}
}