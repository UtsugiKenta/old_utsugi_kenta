package utsugi_kenta.bulletin_board.beans;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {

	private int id;
	private int userId;
	private String text;
	private String title;
	private String category;
	private Date createdDate;
	private Date updatedDate;

	// メッセージのid
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	// メッセージのタイトル
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	// メッセージの内容
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	// メッセージのカテゴリ
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}

	// メッセージを入力したユーザーID
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	// メッセージが作成された日時
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	// メッセージが更新された日時
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}