package utsugi_kenta.bulletin_board.beans;

import java.io.Serializable;
import java.util.Date;

public class UserComment implements Serializable {

	private int commentId;
	private String name;
	private int userId;
	private int messageId;
	private String commentText;
	private Date createdDate;

	// メッセージのid
	public int getId() {
		return commentId;
	}
	public void setId(int commentId) {
		this.commentId = commentId;
	}

	// ユーザーネーム
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// コメントの内容
	public String getText() {
		return commentText;
	}
	public void setText(String commentText) {
		this.commentText = commentText;
	}

	// メッセージを入力したユーザーID
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	// コメントを入力したメッセージのid
	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	// メッセージが作成された日時
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
}
