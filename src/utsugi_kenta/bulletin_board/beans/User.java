package utsugi_kenta.bulletin_board.beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {

	private int id;
	private String account;
	private String password;
	private String name;
	private int branchId;
	private int departmentId;
	private int isStopped;
	private Date createdDate;
	private Date updatedDate;

	// ユーザーID
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	// ユーザーアカウント
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	// ユーザーパスワード
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	// ユーザーネーム
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// 支社のid
	public int getBranchId() {
		return branchId;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	// 部署のid
	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	// アカウントを停止するためのメソッド
	public int getIsStopped() {
		return isStopped;
	}

	public void setIsStopped(int isStopped) {
		this.isStopped = isStopped;
	}

	// ユーザー作成日時
	public Date getCreateDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	// ユーザー更新日時
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}