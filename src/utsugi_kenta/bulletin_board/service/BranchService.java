package utsugi_kenta.bulletin_board.service;

import static utsugi_kenta.bulletin_board.utils.CloseableUtil.*;
import static utsugi_kenta.bulletin_board.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import utsugi_kenta.bulletin_board.beans.Branch;
import utsugi_kenta.bulletin_board.dao.BranchDao;

public class BranchService {
	 public List<Branch> select() {
		Connection connection = null;
		try {
			connection = getConnection();
			List<Branch> branches= new BranchDao().select(connection);

			commit(connection);
			return branches;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
