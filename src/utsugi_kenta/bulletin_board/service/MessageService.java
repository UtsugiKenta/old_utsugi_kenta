package utsugi_kenta.bulletin_board.service;

import static utsugi_kenta.bulletin_board.utils.CloseableUtil.*;
import static utsugi_kenta.bulletin_board.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import utsugi_kenta.bulletin_board.beans.Message;
import utsugi_kenta.bulletin_board.beans.UserMessage;
import utsugi_kenta.bulletin_board.dao.MessageDao;
import utsugi_kenta.bulletin_board.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String userStartDate, String userEndDate , String category) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd k:mm:ss");
		String defaultStartDate = "2021-01-01 00:00:00";
		Date defaultEndDate = new Date();
		String startDate = null;
		String endDate = null;

		if(StringUtils.isBlank(userStartDate)) {
			startDate = defaultStartDate;
		}else {
			startDate = userStartDate + " 00:00:00";
		}

		if(StringUtils.isBlank(userEndDate)) {
			endDate = sdf.format(defaultEndDate).toString();
		}else {
			endDate = userEndDate + " 23:59:59";
		}

		if(!StringUtils.isBlank(category)) {
			category = "%" + category + "%";
		}

		Connection connection = null;
		try {
			connection = getConnection();
			List<UserMessage> messages = new UserMessageDao().select(connection, startDate, endDate, category);

			commit(connection);
			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(Message message) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}