package utsugi_kenta.bulletin_board.service;

import static utsugi_kenta.bulletin_board.utils.CloseableUtil.*;
import static utsugi_kenta.bulletin_board.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import utsugi_kenta.bulletin_board.beans.Department;
import utsugi_kenta.bulletin_board.dao.DepartmentDao;

public class DepartmentService {
	 public List<Department> select() {
		Connection connection = null;
		try {
			connection = getConnection();
			List<Department> departments= new DepartmentDao().select(connection);

			commit(connection);
			return departments;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
