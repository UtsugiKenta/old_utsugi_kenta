<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ログイン画面</title>
		<link href="./style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class=login-list>
			<form action="login" method="post"><br />
				<table class="login">
					<tr>
						<td><label for="account">アカウント</label></td>
						<td><input name="account" id="account" value="${loginAccount}"/></td>
					</tr>
					<tr>
						<td><label for="password">パスワード</label></td>
						<td><input name="password" type="password" id="password"/></td>
					</tr>
					<tr>
						<td><input type="submit" value="ログイン" /></td>
						<td></td>
					</tr>
				</table>
			</form>

			<div class="copyright"> Copyright(c)Utsugi Kenta</div>
			</div>
		</div>
	</body>
</html>