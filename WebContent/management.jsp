<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<title>ホーム画面</title>
	</head>
	<body>
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
			</c:if>

			<div class="link">
				<a href="./">ホーム</a>
				<a href="signup">ユーザー新規登録</a>
			</div><br>

			<div class="userStatus">
				<c:forEach items="${userBranchDepartments}" var="management">

					<div class="userAccount">アカウント：<c:out value="${management.userAccount}"/></div>
					<div class="userName">名前:<c:out value="${management.userName}"/></div>
					<div class="branchName">支社:<c:out value="${management.branchName}"/></div>
					<div class="departmentNmae">部署:<c:out value="${management.departmentName}"/></div>
					<div class="stopStatus">アカウント復活停止状態:<c:out value="${management.stopStatus}"/></div>

					<form action="setting" method="get">
						<input type="hidden" name="userId" value="${ management.userId }"/>
						<input type="submit" value="編集"/>
					</form>

					<c:choose>
						<c:when test="${ management.userId == loginUser.id}">
							<c:choose>
								<c:when test="${management.isStopped == 1}">
									<form action="stop" method="post">
										<input type="hidden" name="userId" value="${management.userId}"/>
										<input type="submit" value="復活" disabled/>
									</form>
								</c:when>
								<c:otherwise>
									<form action="stop" method="post">
										<input type="hidden" name="userId" value="${management.userId}"/>
										<input type="submit" value="停止" disabled/>
									</form>
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${management.isStopped == 1}">
									<form action="stop" method="post">
										<input type="hidden" name="userId" value="${management.userId}"/>
										<input type="hidden" name="isStopped" value="0"/>
										<input type="submit" value="復活" onclick="return confirm('本当に復活させますか？')"/>
									</form>
								</c:when>
								<c:otherwise>
									<form action="stop" method="post">
										<input type="hidden" name="userId" value="${management.userId}"/>
										<input type="hidden" name="isStopped" value="1"/>
										<input type="submit" value="停止" onclick="return confirm('本当に停止させますか？')"/>
									</form>
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose><br>
				</c:forEach>
			</div>
			<div class="copyright"> Copyright(c)Utsugi Kenta</div>
		</div>
	</body>
</html>
