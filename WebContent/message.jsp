<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<title>新規投稿画面</title>

	</head>
	<body>
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="link">
				<a href="./">ホーム</a>
			</div>

			<div class="form-area">
				<form action="message" method="post">
					<table class="massage">
						<tr>
							<td><label for="title">件名:</label></td>
							<td><input name="title" id="title" value="${message.title}"/></td>
						</tr>
						<tr>
							<td><label for="category">カテゴリ:</label></td>
							<td><input name="category" id="category" value="${message.category}"/></td>
						</tr>
						<tr>
							<td><label for="text">投稿内容:</label></td>
							<td><textarea name="text" cols="50" rows="10" class="bulletin-board"><c:out value="${message.text}" /></textarea></td>
						</tr>
						<tr>
							<td><input type="submit" value="投稿"></td>
							<td></td>
						</tr>
					</table>
				</form>
			</div>
			<div class="copyright">Copyright(c)Utsugi Kenta</div>
		</div>
	</body>
</html>